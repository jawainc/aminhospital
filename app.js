let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieSession = require('cookie-session');
let bodyParser = require('body-parser');
let helmet = require('helmet');
let debug = require('debug')('Project:server');
let http = require('http');
let _ = require('lodash');
let PatientEntry = require('./models/PatientEntry');
let Discount = require('./models/Discount');
let CheckDB = require('./models/Table');
let IOSession = require('./models/Session');
const config = require('./config/config');

// Models
let pe = new PatientEntry();
let discount = new Discount();
let SocketUsers = require('./models/SocketUsers');
let soketUsers = new SocketUsers();

let app = express();

/********************************
 * ******************************
 * Security
 * ******************************
 *******************************/
app.use(helmet());
let expiryDate = new Date(Date.now() + 60 * 60 * 1000); // 1 hour
let session = cookieSession({
    name: 'session',
    keys: [config.hash.salt1, config.hash.salt2],
    resave: true,
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
        expires: expiryDate
    }
});
app.use(session);

/****************************
 *****************************
 Sub Modules/Apps
 *****************************
 ****************************/

let login = require('./app_modules/login/index');
let admin =require('./app_modules/admin/index');
let reception =require('./app_modules/reception/index');
let doctor =require('./app_modules/doctor/index');
let assistant =require('./app_modules/assistant/index');
let lab =require('./app_modules/lab/index');
let pharmacy =require('./app_modules/pharmacy/index');
let auth =require('./app_modules/auth/index');
let dictionary =require('./app_modules/dictionary/index');
let signout =require('./app_modules/signout/index');
let patient = require('./app_modules/patient/index');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


/****************************
 *****************************
 Setting Sub Modules/App
 *****************************
 ****************************/

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth");
    res.header("Access-Control-Expose-Headers", "x-auth");

    if ('OPTIONS' == req.method) {
        res.writeHead(204);
        res.end();
    }
    else {
        next();
    }


});

app.get('/', function (req, res) {
    res.render('index');
});
app.use('/auth', auth);
app.use('/login', login);
app.use('/admin', admin);
app.use('/reception', reception);
app.use('/assistant', assistant);
app.use('/doctor', doctor);
app.use('/dictionary', dictionary);
app.use('/signout', signout);
app.use('/patient', patient);
// app.use('/lab', lab);
// app.use('/pharmacy', pharmacy);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
if (app.get('env') === 'production') {
    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
}

/**
 * checking for Db tables
 */
let checkDb = new CheckDB();
checkDb.checkDB();
let ioSession = new IOSession();

/**
 * Get port from environment and store in Express.
 */

let port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

let server = http.createServer(app);
// Sockets
let io = require('socket.io')(server);
let get_changes_registered = false;
let get_changes_discount = false;

io.on('connection', function (socket) {
    socket.on('registerUser', function (data) {
        if(_.hasIn(data, 'doctor_id') && _.hasIn(data, 'department_id')) {
            let user = soketUsers.addUser(socket.id, data.doctor_id, data.department_id, 'Patient');
            console.log('user added: ', user);
            if (!get_changes_registered) {
                pe.getChanges(io, soketUsers);
                get_changes_registered = true;
            }
        }
    });
    socket.on('registerDiscount', function () {
        console.log('registerDiscount');
            let user = soketUsers.addUser(socket.id, null, null, 'Discount');
            console.log('user added: ', user);
            if (!get_changes_discount) {
                discount.getChanges(io, soketUsers);
                get_changes_discount = true;
            }
    });
    socket.on('disconnect', () => {
        let user = soketUsers.removeUser(socket.id);
    });
});




/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    let port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    let bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    let addr = server.address();
    let bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Node Listening on ' + bind);
    debug('Listening on ' + bind);
}


 // module.exports = app;