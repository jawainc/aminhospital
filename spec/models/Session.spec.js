let Session = require('../../models/Session');

let session = new Session();

describe('This will test session functionality', ()=> {
   before (function () {
      sinon.spy(session, "addSession");
   });
   it('should store new session', ()=> {
      let obj = {
        io: 'random strning',
        user_id: 'user id'
      };
      session.addSession(obj);
      expect(session.addSession.calledTwice);
      expect(session.addSession.calledWith('message'));
   });
   it.skip ('should load session from DB', ()=> {});
});