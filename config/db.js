let config = require('./config');
// let db_migrate = require("./db_migrate");

let r = require('rethinkdbdash')({
    host: config.db.host,
    db: config.db.db,
    port: config.db.port
});


// First Run for tables creation
//
// r.dbList().run().then((result)=>{
//     if(result.indexOf(config.db.db) < 0 )
//         r.dbCreate(config.db.db).run().then((result) => {
//             console.log(result);
//         } )
// });
//
// for(i=0; i<db_migrate.tables.length; i++){
//     r.tableCreate(db_migrate.tables[i]).run().then((result)=>{
//         if(result.tables_created)
//             console.log("Table Created: "+db_migrate.tables[i]);
//         else
//             console.log("Fail: "+db_migrate.tables[i]);
//
//     });
// }



module.exports = r;