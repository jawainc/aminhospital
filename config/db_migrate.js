module.exports = {
    tables: [
        'departments',
        'dictionaries',
        'discounts',
        'doctors',
        'employees',
        'expanses',
        'incomes',
        'patients',
        'patient_entries',
        'users'
    ]
};