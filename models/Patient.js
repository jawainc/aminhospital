/*********************
 *
 * Patient Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'patients';
const timestamps = true;

class Patient extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    addPatient(data){

        let patient_id = data.id;
        if(!this.lodash.isNil(patient_id) && !this.lodash.isEmpty(patient_id)){

            return this.updatePatient(patient_id, data).then( (result) => {
                return Promise.resolve(patient_id);
            }).catch( (err) => {
                return Promise.reject(err);
            } )

        } else{
            return this.findPatient(data.phone, data.name).then( (result) => {
                if(!this.lodash.isEmpty(result)){
                    let first_patient = result[0];
                    return this.updatePatient(first_patient.id, data).then( (result) => {
                        return Promise.resolve(first_patient.id);
                    }).catch( (err) => {
                        return Promise.reject(err);
                    } )
                } else{
                    return this.insertPatient(data).then( (result) => {
                        if(result.inserted){
                            return Promise.resolve(result.generated_keys[0]);
                        }
                        else
                            return Promise.reject(new Error('fail'));
                    })
                        .catch( (err) => {
                            return Promise.reject(err);
                        });
                }
            })
                .catch((err) => {
                    return Promise.reject(err);
                });
        }
    }
    findPatient(phone,name){
        return this.db.table(table).filter( (patient) => {
            return patient('name').match(`(?i)^${name.trim()}$`).and(patient('phone').eq(phone));
        });
    }
    updatePatient(id, data){
        return this.update(id, {
            age: data.age,
            gender: data.gender,
            nic: data.nic,
            location: data.location
        });
    }
    insertPatient(data){
        return this.insert({
            phone: data.phone,
            name: data.name.trim(),
            age: data.age,
            gender: data.gender,
            nic: data.nic,
            location: data.location
        });
    }
    async getPhoneData (data) {
        try {
            let result = await this.db.table('patients').group('phone').filter((p) => {
                return p('phone').match("(?i)"+"^"+data.phone);
            }).merge((row) => {
                return {
                    'last_visit': this.db.table('patient_entries').getAll(row('id'), {index: 'patient_id'})
                        .orderBy('created_at').isEmpty()
                }
            }).run();

                return Promise.resolve(result);

        } catch (e) {
            return Promise.reject(e);
        }

    }
}
module.exports = Patient;