/*********************
 *
 * Department Income Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'department_incomes';
const timestamps = true;

class DepartmentIncome extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    addIncome(data){
        return this.insert(data).then( (result) => {
            if(result.inserted)
                return Promise.resolve(true);
            else
                return Promise.reject(new Error('fail'));
        })
            .catch((err)=>{
                Promise.reject(err);
            })
    }



}

module.exports = DepartmentIncome;