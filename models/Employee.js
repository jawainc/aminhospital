/*********************
 *
 * Employee Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'employees';

// timestamps
const timestamps = true;


class Employee extends BaseModel {
    constructor() {
        super(table, timestamps);
    }

    // validations
    get validates(){
        return {
            name: {
                presence: {message: "^Name is required"}
            }
        };
    }
}



module.exports = Employee;