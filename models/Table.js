/*********************
 *
 * Table Model
 * check for tables and create them
 *
 *********************/
let BaseModel = require('./BaseModel');

let db_migrate = require('../config/db_migrate');

class Table extends BaseModel {
    constructor() {
        super('');
    }

    checkDB () {
        this.db(db_migrate.tables).difference(this.db.tableList())
            .forEach(table => this.db.tableCreate(table))
            .run();

        this.db('amin_hospital').table('patient_entries').indexList().do(
             (result) => {
                return this.db.branch(
                    result.isEmpty(),
                    ['created_at', 'patient_id'].map((val) => {
                        return this.db.table('patient_entries').indexCreate(val, {multi: true})
                    }),
                    null
                )
            }
        ).run();

        this.db('amin_hospital').table('incomes').indexList().do(
            (result) => {
                return this.db.branch(
                    result.isEmpty(),
                    ['created_at', 'doctor_id'].map((val) => {
                        return this.db.table('incomes').indexCreate(val, {multi: true})
                    }),
                    null
                )
            }
        ).run();

        this.db('amin_hospital').table('expanses').indexList().do(
            (result) => {
                return this.db.branch(
                    result.isEmpty(),
                    ['created_at'].map((val) => {
                        return this.db.table('expanses').indexCreate(val, {multi: true})
                    }),
                    null
                )
            }
        ).run()
    }
}



module.exports = Table;