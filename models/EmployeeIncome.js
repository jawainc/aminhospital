/*********************
 *
 * Employee Income Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'employee_incomes';
const timestamps = true;

class EmployeeIncome extends BaseModel {

    constructor() {
        super(table, true);
    }

    addIncome(data){
        return this.insert(data).then( (result) => {
            if(result.inserted)
                return Promise.resolve(true);
            else
                return Promise.reject(new Error('Employee Income Fail'));
        })
            .catch((err)=>{
                Promise.reject(err);
            })
    }



}

module.exports = EmployeeIncome;