/*********************
 *
 * Doctor Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'doctors';


class Doctor extends BaseModel {
    constructor() {
        super(table,true);
    }


    get validates(){
        return {
            name: {
                presence: {message: "^Name is required"}
            }
        };
    }


    get has_many_embeded(){
        return 'employees'
    }

}



module.exports = Doctor;