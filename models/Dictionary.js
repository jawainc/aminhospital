/*********************
 *
 * User Model
 *
 *********************/
let BaseModel = require('./BaseModel');
let _ = require('lodash');

// table name
const table = 'dictionaries';
const timestamps = false;

class Dictionary extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    async search (data) {

        let col = "";
        let val = "";
        _.mapKeys(data, (value,key) => {
            col = key;
            val = value;
        });
        let dt = await this.db.table(this.table).filter((p) => {
            return p('dictionary').eq(col).and(p('value').match("(?i)"+val));
        }).run();
        return dt;
    }

    async add (data) {
        try {
            let dt = await this.db.table(this.table).filter(data).isEmpty()
                .do(empty => this.db.branch(
                    empty,
                    this.db.table(this.table).insert(data),
                    null
                ));
            return true;
        } catch (e) {
            return e;
        }
    }


}

module.exports = Dictionary;