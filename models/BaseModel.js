/*********************
 *
 * Base Model
 * every other model should extend it
 *
 *********************/
var _ = require('lodash');
var validate = require("validate.js");
var r = require('../config/db');

var _table = Symbol();
var _timestamps = Symbol();

/*********************
 *
 * Validation
 * if validates method defined in child class
 *
 *********************/
let _validation = Symbol();


/*********************
 *
 * Data
 *
 *********************/
let _data = Symbol();


class BaseModel {



    constructor(model_table, timestamps=false){
        this.table = model_table;
        this.timestamps = timestamps;
    }

    /*********************
     *
     * Getters/Setters
     *
     *********************/
    get table(){
        return this[_table];
    }

    set table(value){
        this[_table] = value;
    }
    get timestamps(){
        return this[_timestamps];
    }
   set timestamps(value){
        this[_timestamps] = value;
   }

   get data(){
       return this[_data];
   }
   set data(val){
       this[_data] = val;
   }

   get db(){
       return r;
   }
   get lodash(){
       return _;
   }

    /**
     *
     * @param data
     * @returns {*}
     */
    insert(data){


        this.data = data;


        let result = this.doValidation();

        if(!_.isNil(result))
            return Promise.reject(result);

        if (this.timestamps)
            this.insertTimestamps();


        if(!_.isNil(this.unique)){
            return this.uniqueInsert()
                .then( (result) => {

                    if(result.status == "Fail"){
                        return Promise.reject(result.msg);
                    }
                    else{
                        return r.table(this.table).insert(this.data).run();
                    }
                })
                .catch((err) => {
                    return Promise.reject(err);
                });
        }
        else
            return r.table(this.table).insert(this.data).run();



    }

    /**
     *
     * @param id
     * @param data
     * @returns {*}
     */
    update(id, data){

        this.data = data;

        let result = this.doValidation();

        if(!_.isNil(result))
            return Promise.reject(result);

        if (this.timestamps)
            this.updateTimestamp();

        if(!_.isNil(this.unique)){
            return this.uniqueUpdate()
                .then( (result) => {

                    if(result.status == "Fail"){
                        return Promise.reject(result.msg);
                    }
                    else{
                        return r.table(this.table).get(id).update(data, {
                            returnChanges: true
                        }).run();
                    }
                })
                .catch((err) => {
                    return Promise.reject(err);
                });
        }
        else
            return r.table(this.table).get(id).update(data, {
                returnChanges: true
            }).run();

    }

    /**
     * Update data
     *
     * @param where
     * @param data
     * @returns {*}
     */
    update_where(where, data){

        this.data = data;

        if(this.timestamps)
            this.updateTimestamp();

        return r.table(this.table).filter(where).update(this.data, {
            returnChanges: true
        }).run();
    }


    /**
     * Find Single Record By Primary Key
     *
     * @param id
     * @returns Promise
     */
    find(id){
        return r.table(this.table).get(id).run();
    }

    /**
     * 
     * @param id
     * @returns {*}
     */
    find_with_relation_embeded(id){

        if(_.isNil(this.has_many_embeded))
            return Promise.reject(new Error("No relation defined"));


        let tables = this.has_many_embeded;

        return r.table(this.table).get(id)
            .do( (dep) => {
                return r.branch(
                    dep.hasFields(tables),
                    dep.merge({[tables]: dep(tables).map((doc) => {
                        return doc.merge((r.table(tables).get(doc('id')).without('percentage')))
                    })}),
                    dep.merge({[tables]: []})

                )

            })
    }

    /**
     *
     * @param id
     * @returns {*}
     */
    find_where_relation_embeded(filter ={}, selectfield){

        if(_.isNil(this.has_many_embeded))
            return Promise.reject(new Error("No relation defined"));


        let tables = this.has_many_embeded;

        return r.table(this.table).filter(filter)(selectfield)
            .do( (dep) => {
                return r.branch(
                    dep.hasFields(tables),
                    dep.merge({[tables]: dep(tables).map((doc) => {
                        return doc.merge((r.table(tables).get(doc('id')).without('percentage')))
                    })}),
                    dep.merge({[tables]: []})

                )

            })
    }

    /**
     * Get All With Embedded
     * @returns {*}
     */
    find_all_with_relation_embedded(){

        if(_.isNil(this.has_many_embeded))
            return Promise.reject(new Error("No relation defined"));

        let tables = this.has_many_embeded;

        return r.table(this.table)
            .map( (dep) => {
                return r.branch(
                    dep.hasFields(tables),
                    dep.merge({[tables]: dep(tables).map((doc) => {
                        return doc.merge((r.table(tables).get(doc('id')).without('percentage')))
                    })}),
                    dep.merge({[tables]: []})

                )

            });
    }

    /**
     * Get All Record
     *
     * @returns Promise
     */
    get(order_by = {}) {

        if(!_.isEmpty(order_by)){


            if(order_by.order == "desc")
                return r.table(this.table).orderBy(r.desc(order_by.col)).run();
            else
                return r.table(this.table).orderBy(r.asc(order_by.col)).run();
        }
        else
            return r.table(this.table).run();
    }


    /**
     * Get filtered record
     *
     * @param where
     * @returns {*}
     */
    filter(where){
        return r.table(this.table).filter(where).run();
    }

    like_start(where){

        let col = "";
        let val = "";
        _.mapKeys(where, (value,key) => {
            col = key;
            val = value;
        });


        // return r.table(this.table).group(col).run();

        return r.table(this.table).group(col).filter((p) => {
            return p(col).match("(?i)"+"^"+val);
        }).run();
    }

    like_end(where){

        let col = "";
        let val = "";
        _.mapKeys(where, (value,key) => {
            col = key;
            val = value;
        });

        return r.table(this.table).filter((p) => {
            return p(col).match("(?i)"+val+"$");
        }).run();
    }

    like(where){

        let col = "";
        let val = "";
        _.mapKeys(where, (value,key) => {
            col = key;
            val = value;
        });

        return r.table(this.table).filter((p) => {
            return p(col).match("(?i)"+val);
        }).run();
    }

    contains(where){
        let col = "";
        let val = "";
        _.mapKeys(where, (value,key) => {
            col = key;
            val = value;
        });

        return r.table(this.table).filter((p) => {
            return p(col).contains(val);
        }).run();
    }

    /**
     *
     * @param where
     * @param ne  (Not Equal)
     * @returns {*}
     */
    counts(where = {}, ne = {}){
        if(_.isEmpty(ne)){
            return r.table(this.table).filter(where).count().run();
        }
        else{
            return r.table(this.table).filter(
                r.row("id").ne(ne.id).and(r.row(where.key).eq(where.val)
            ))
                .count().run();
        }
    }

    /**
     *
     * @param id
     * @returns {*}
     */
    delete(id){
        return r.table(this.table).get(id).delete({returnChanges: true}).run();
    }

    /**
     *
     * @param where
     * @returns {*}
     */
    delete_where(where){
        return r.table(this.table).filter(where).delete({returnChanges: true}).run();
    }

    /**
     *
     * @param data
     * @returns {*}
     */
    checkUniqueInsert(data){


        var scope = this;
        var where = {};
        _.forEach(scope.unique, function(val, key_unique) {

            _.mapKeys(data, function(value, key) {

                if(key_unique == key){
                    where[key] = value;
                }

            });


        });


        return this.counts(where);
    }

    /**
     *
     * @param data
     * @returns {*}
     */
    checkUniqueUpdate(data, id){


        var scope = this;
        var where = {
            key: '',
            val: ''
        };
        _.forEach(scope.unique, function(val, key_unique) {

            _.mapKeys(data, function(value, key) {

                if(key_unique == key){
                    where.key = key;
                    where.val = value;
                }

            });


        });


        return this.counts(where, {id: id});
    }


    /**
     *
     * @param data
     * @returns {{}}
     */
    insertTimestamps(){
        this.data.created_at = r.now().inTimezone('+05:00');
        this.data.updated_at = r.now().inTimezone('+05:00');
    }

    /**
     *
     * @param data
     * @returns {{}}
     */
    updateTimestamp(){
        this.data.updated_at = r.now().inTimezone('+05:00');
    }


    /**
     *
     * @returns {null}
     */
    doValidation(){
        if(!_.isNil(this.validates))
            return  validate(this.data, this.validates, {format: "flat"});
        else
            return null;
    }

    /**
     *
     * @returns {Promise.<T>}
     */
    uniqueInsert() {

        var scope = this;
        var where = {};
        var reject_msgs = "";
        var default_message = "Already Exists";


        _.forEach(scope.unique, function(val, key_unique) {

            _.mapKeys(scope.data, function(value, key) {

                if(key_unique == key){

                   where[key] = value;
                   if(!_.isNil(scope.unique[key].message)){

                       if(_.isEmpty(reject_msgs))
                            reject_msgs += `${scope.unique[key_unique].message}`;
                       else
                           reject_msgs += `\n${scope.unique[key_unique].message}`;

                   }
                   else{

                       if(_.isEmpty(reject_msgs))
                           reject_msgs += `${_.upperFirst(key_unique)}: ${default_message}`;
                       else
                           reject_msgs += `\n${_.upperFirst(key_unique)}: ${default_message}`;

                   }

                }

            });

        });


        return this.counts(where)
            .then((result) => {
                if(result == 0)
                    return Promise.resolve({status: "OK"});
                else{
                    return Promise.reject({
                        status: "Fail",
                        msg: reject_msgs
                    });
                }

            })
            .catch((err) => {return err});
    }

    /**
     *
     * @returns {Promise.<T>}
     */
    uniqueUpdate() {

        var scope = this;
        var where = {};
        var reject_msgs = "";
        var default_message = "Already Exists";


        _.forEach(scope.unique, function(val, key_unique) {

            _.mapKeys(scope.data, function(value, key) {

                if(key_unique == key){

                    where={
                        key : key_unique
                    };
                    if(!_.isNil(scope.unique[key].message)){

                        if(_.isEmpty(reject_msgs))
                            reject_msgs += `${scope.unique[key_unique].message}`;
                        else
                            reject_msgs += `\n${scope.unique[key_unique].message}`;

                    }
                    else{

                        if(_.isEmpty(reject_msgs))
                            reject_msgs += `${_.upperFirst(key_unique)}: ${default_message}`;
                        else
                            reject_msgs += `\n${_.upperFirst(key_unique)}: ${default_message}`;

                    }

                }

            });

        });




        return this.counts(where, {id: this.data.id})
            .then((result) => {


                if(result == 0)
                    return Promise.resolve({status: "OK"});
                else{
                    return Promise.reject({
                        status: "Fail",
                        msg: reject_msgs
                    });
                }

            })
            .catch((err) => {return err});

    }


}

module.exports = BaseModel;