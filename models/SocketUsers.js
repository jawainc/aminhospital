let _ = require('lodash');

class SocketUsers {
    constructor () {
        this.users = [];
    }
    addUser (id, doctor_id, department_id, type) {
        let user = _.find(this.users, {id: id, doctor_id: doctor_id, department_id: department_id, type: type});
        if (_.isNil(user)) {
            user = {id: id, doctor_id: doctor_id, department_id: department_id, type: type};
            this.users.push(user);
        }
        return user;
    }

    removeUser (id) {
        let user = this.getUser(id);
        if (user) {
            this.users = this.users.filter((user) => user.id !== id);
        }
        return user;
    }
    getUser (id) {
        return this.users.filter((user) => user.id === id)[0]
    }
    getUserList (doctor_id, department_id) {
        let idsArray = [];
        let users = _.filter(this.users, {doctor_id: doctor_id, department_id: department_id});
        if (!_.isNil(users)) {
            idsArray = _.map(users, 'id');
        }
        return idsArray;
    }

    getDiscountUserList () {
        let idsArray = [];
        let users = _.filter(this.users, {type: 'Discount'});
        if (!_.isNil(users)) {
            idsArray = _.map(users, 'id');
        }
        return idsArray;
    }
}

module.exports = SocketUsers;