/*********************
 *
 * Expanse Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'expanses';
const timestamps = true;

class Patient extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    async getExpanseDetails (from, to) {
        try {
            let data = await this.db.table(table).orderBy({index: this.db.desc('created_at')}).filter(
                this.db.row('created_at').date().during(
                    this.db.time(from.getFullYear(), from.getMonth()+1, from.getDate(), "+05:00"),
                    this.db.time(to.getFullYear(), to.getMonth()+1, to.getDate(), "+05:00"),
                    {rightBound: "closed"}
                )
            ).run();
            return Promise.resolve(data);

        } catch (e) {
            return Promise.reject(e)
        }
    }
}

module.exports = Patient;