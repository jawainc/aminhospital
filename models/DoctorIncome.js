/*********************
 *
 * Doctor Income Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'doctor_incomes';
const timestamps = true;

class DoctorIncome extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    addIncome(data){
        return this.insert(data).then( (result) => {
            if(result.inserted)
                return Promise.resolve(true);
            else
                return Promise.reject(new Error('fail'));
        })
            .catch((err)=>{
                Promise.reject(err);
            })
    }



}

module.exports = DoctorIncome;