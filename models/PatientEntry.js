/*********************
 *
 * Patient Entries Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'patient_entries';
const timestamps = true;

class PatientEntry extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    getChanges (io, socketUsers) {
        this.db.table(table)
            .filter({type: 'Out Door'})
            .changes()
            .merge( (row) => {
                return this.db.branch(
                    row('new_val')('status').ne('Completed'),
                    {'patient': this.db.table('patients').get(row('new_val')('patient_id')).pluck('name')},
                    {'patient': null}
                );
            })
            .run()
            .then(function(feed){
                feed.each(function(err, item){
                    if (err) {
                        console.log(err);
                        return;
                    } else if (item && item.new_val && item.new_val.status !== 'Completed') {
                        let dt = item.new_val;
                        dt.patient = {
                            name: item.patient.name
                        };
                        let users = socketUsers.getUserList(dt.doctor_id, dt.department_id);
                        if (users) {
                            console.log(users);
                            users.forEach((id) => {
                                console.log('starting feed');
                                io.to(id).emit('patientChannel', dt);
                            })
                        }
                    }
                })
            })
            .catch((error) => {
                console.log(error);
            });
    }

    async getLastVisit(id) {
        try {
            let result = await this.db.table(table).getAll(id, {index: 'patient_id'})
                .orderBy('created_at').nth(-1).pluck('created_at')
            return Promise.resolve(result);
        } catch (e) {
            return Promise.reject(e);
        }
    }

    getDoctorPatients(doctor_id, department_id){

        return this.db.table(table).filter(
            this.db.row("type").eq('Out Door')
                .and(this.db.row("doctor_id").eq(doctor_id))
                .and(this.db.row("department_id").eq(department_id))
                .and(this.db.row("status").ne("Completed"))
            ).orderBy(this.db.asc('created_at')).merge( (row) => {
            return {
                'patient': this.db.table('patients').get(row('patient_id')).pluck('name')
            }
        }).pluck('id', 'status', 'doctor_id', 'department_id', 'created_at', 'patient').then( (data) => {

            return Promise.resolve({
                doctor_id: doctor_id,
                department_id: department_id,
                data: data
            });
        })
            .catch((err) => Promise.reject(err));

    }

    getDoctorPatientDetail(id){

        return this.db.table(table).get(id).merge( (row) => {
            return {
                'patient': this.db.table('patients').get(row('patient_id')),
                'doctor': this.db.table('departments').get(row('department_id')).do((doc) => {
                    return doc('doctors').filter({id: row('doctor_id')}).nth(0).pluck('assistant_form', 'doctor_form')
                })
            }
        }).then( (data) => {
            return Promise.resolve(data);
        })
            .catch((err) => Promise.reject(err));

    }

    getAssistantPatientDetail(id){

        return this.db.table(table).filter({
            type: 'Out Door',
            id: id
        }).merge( (row) => {
            return {
                'patient': this.db.table('patients').get(row('patient_id')),
                'doctor': this.db.table('departments').get(row('department_id')).pluck('doctors')
            }
        }).then( (data) => {
            let dt = data[0];
            let doc = dt.doctor.doctors;
            doc = this.lodash.find(doc, {id: dt.doctor_id});
            if (doc) {
                dt.assistant = doc.assistant_form;
            }
            return Promise.resolve(dt);
        })
            .catch((err) => Promise.reject(err));

    }

    getPatientHistory (id) {
        return this.db.table(table).filter({patient_id: id, status: 'Completed'})
            .orderBy(this.db.desc('created_at'))
            .merge( (row) => {
                return {
                    'patient': this.db.table('patients').get(row('patient_id')),
                    'department': this.db.table('departments').get(row('department_id'))
                        .merge((d_row) => {
                            return {
                                'doctor': d_row('doctors').filter({id: row('doctor_id')})
                                    .pluck('assistant_form', 'doctor_form', 'name').nth(0)
                            }
                        }).pluck('name', 'doctor')
                }
            })
            .then(data => {
            return Promise.resolve(data);
        }).catch((err) => Promise.reject(err));
    }



}

module.exports = PatientEntry;