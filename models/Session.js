/*********************
 *
 * Sessions Model
 *
 *********************/
let BaseModel = require('./BaseModel');
let _ = require('lodash');
// table name
const table = 'sessions';
const timestamps = false;

class Session extends BaseModel{
    constructor () {
        super (table, timestamps);
    }

    /**
     * adds/update an io session
     * obj must have key 'io'
     * @param obj
     * @returns {Promise<*>}
     */
    async addSession (obj) {
        if (_.isObject(obj) && _.has(obj, 'io')) {
            let result = await this.db.table(table).filter({io: obj.io})
                .isEmpty().branch(
                    this.db.table(table).insert(obj),
                    this.db.table(table).filter({io: obj.io}).update(obj)
                ).run();
            if (result) {
                return Promise.resolve();
            } else {
                return Promise.reject('Error in insert');
            }
        } else {
            return Promise.reject('Invalid Value');
        }
    }

    /**
     * fetch session records
     * @param obj
     * @returns {Promise<*>}
     */
    async getSession (obj) {
        if (_.has(obj, 'io')) {
            let result = await this.db.table(table).filter(obj).run();

        } else {
            return Promise.reject('Invalid Request');
        }
    }

    /**
     * remove sessions
     * @param io
     * @returns {Promise<*>}
     */
    async removeSession (io) {
        if (!_.isNil(io)) {
            let result = await this.db.table(table).filter({io: io}).delete().run();
            if (result) {
                return Promise.resolve();
            } else {
                return Promise.reject('No Delete');
            }
        } else {
            return Promise.reject('Invalid Request');
        }
    }
}

module.exports = Session;