/*********************
 *
 * Department Model
 *
 *********************/
var BaseModel = require('./BaseModel');
var _ = require('lodash');
// table name
const table = 'departments';



class Department extends BaseModel {
    constructor() {
        super(table);
    }


    get validates(){
        return {
            name: {
                presence: {message: "^Name is required"}
            }
        };
    }

    get unique(){
        return {
            name: {message: "Name Already Exists"}
        }
    }


    get has_many_embeded(){
        return 'doctors'
    }

    /**
     * gets departments data with doctor form and assistants
     */
    getDepartmentsForReception () {
       return this.db.table(table).without({'doctors': 'percentage'},{'doctors': 'doctor_form'}, {'doctors': 'assistants'}).run();
    }

    async hasAssistantForm (department_id, doctor_id) {
        try {
            let data = await this.db.table(table).get(department_id).getField('doctors')
                .filter({id: doctor_id}).pluck('assistant_form').isEmpty();

            return data;
        } catch (e) {
            return false;
        }
    }

}



module.exports = Department;