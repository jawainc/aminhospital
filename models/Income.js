/*********************
 *
 * Income Model
 *
 *********************/
var BaseModel = require('./BaseModel');
// table name
const table = 'incomes';
const timestamps = true;

class Patient extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    addIncome(data){
        return this.insert(data).then( (result) => {
            if(result.inserted)
                return Promise.resolve(result.generated_keys[0]);
            else
                return Promise.reject(new Error('fail'));
        })
            .catch((err)=>{
                Promise.reject(err);
            })
    }

    async getDashData(id) {
        try {
            let dash = await this.db.table(table).filter((row) => {
                return row('doctor_id').eq(id).and(row('created_at').month().eq(this.db.now().month()))
            }).pluck('created_at', 'doctor_share')
                .group([this.db.row('created_at').day(), this.db.row('created_at').month()]).map(function(row) {
                return {
                    Share: row('doctor_share'),
                    count: 1
                }
            }) .reduce(function(left, right) {
                let share = left('Share').add(right('Share'));
                let cnt = left('count').add(right('count'));

                return {
                    Share: share,
                    count: cnt
                }
            }).run();
            return Promise.resolve(dash);
        } catch (e) {
            return Promise.reject(e)
        }
    }

    async getBalance (from, to) {
        try {
            let data = await this.db.table(table).filter(
                this.db.row('created_at').date().during(
                        this.db.time(from.getFullYear(), from.getMonth()+1, from.getDate(), "+05:00"),
                        this.db.time(to.getFullYear(), to.getMonth()+1, to.getDate(), "+05:00"),
                        {rightBound: "closed"}
                    )
            ).sum('total_amount').do((income) => {
                return {
                    'income': income,
                    'expanse': this.db.table('expanses').filter((row) => {
                        return row('created_at').date().during(
                            this.db.time(from.getFullYear(), from.getMonth()+1, from.getDate(), "+05:00"),
                            this.db.time(to.getFullYear(), to.getMonth()+1, to.getDate(), "+05:00"),
                            {rightBound: "closed"}
                            )
                    }).sum('amount'),
                    'date': this.db.now().inTimezone('+05:00')
                }
            }).run();
            return Promise.resolve(data);

        } catch (e) {
            return Promise.reject(e)
        }
    }

    async getDeposit (from, to) {
        try {
            let data = await this.db.table(table).orderBy({index: this.db.desc('created_at')}).filter(
                this.db.row('created_at').date().during(
                    this.db.time(from.getFullYear(), from.getMonth()+1, from.getDate(), "+05:00"),
                    this.db.time(to.getFullYear(), to.getMonth()+1, to.getDate(), "+05:00"),
                    {rightBound: "closed"}
                )
            ).pluck('created_at', 'discount', 'total_amount', 'patient_entry_id', 'doctor_id', 'department_id')
                .merge((row) => {
                    return {
                        'patient': this.db.table('patient_entries').get(row('patient_entry_id'))
                            .pluck('patient_id', 'type').do((item) => {
                                return {
                                    'name': this.db.table('patients').get(item('patient_id')).pluck('name'),
                                    'type': item('type')
                                }
                            }),
                        'doctor': this.db.table('doctors').get(row('doctor_id')).pluck('name'),
                        'department': this.db.table('departments').get(row('department_id')).pluck('name'),
                    }
                })
                .run();
            return Promise.resolve(data);

        } catch (e) {
            return Promise.reject(e)
        }
    }

    async getExpanse (from, to) {
        try {
            let data = await this.db.table(table).orderBy({index: this.db.desc('created_at')}).filter(
                this.db.row('created_at').date().during(
                    this.db.time(from.getFullYear(), from.getMonth()+1, from.getDate(), "+05:00"),
                    this.db.time(to.getFullYear(), to.getMonth()+1, to.getDate(), "+05:00"),
                    {rightBound: "closed"}
                )
            ).pluck('created_at', 'discount', 'total_amount', 'patient_entry_id', 'doctor_id', 'department_id')
                .merge((row) => {
                    return {
                        'patient': this.db.table('patient_entries').get(row('patient_entry_id'))
                            .pluck('patient_id', 'type').do((item) => {
                                return {
                                    'name': this.db.table('patients').get(item('patient_id')).pluck('name'),
                                    'type': item('type')
                                }
                            }),
                        'doctor': this.db.table('doctors').get(row('doctor_id')).pluck('name'),
                        'department': this.db.table('departments').get(row('department_id')).pluck('name'),
                    }
                })
                .run();
            return Promise.resolve(data);

        } catch (e) {
            return Promise.reject(e)
        }
    }

}

module.exports = Patient;