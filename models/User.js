/*********************
 *
 * User Model
 *
 *********************/
let BaseModel = require('./BaseModel');
let _ = require('lodash');
const jwt = require('jsonwebtoken');
var CryptoJS = require("crypto-js");
let {hash} = require('../config/config');

// table name
const table = 'users';
const timestamps = true;
const loginFailError = 'Invalid username or password';

class User extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    /**
     * setting login_id to unique
     * it will be checked before every insert or update
     *
     * @returns {{login_id: {message: string}}}
     */
    get unique(){
        return {
            login_id: {message: "Login Id Already Exists"}
        }
    }

    /**
     * login user into system
     * @param data
     * @returns {*}
     */
    async login (data) {
        if(!_.isNil(data.login_id) && !_.isEmpty(data.login_id.trim()) && !_.isNil(data.password)) {
            let login_id = data.login_id.trim();
            let password = data.password;
            try{
                let dbUser = await this.filter({login_id: data.login_id});

                if (!_.isEmpty(dbUser)) {
                    let user = dbUser[0];
                    let pass = await this.passCompare(password, user.password);
                    let result = await this.generateAuthToken(user);
                    return result;
                } else {
                    throw new Error(loginFailError);
                }
            } catch (e){
                throw new Error(e.message);
            }

        } else {
            throw new Error(loginFailError);
        }
    }

    /**
     * password compare
     *
     * @param pass
     * @param hash
     * @returns {*}
     */
    async passCompare (pass, hash) {
        try {
            let resultHash = await this.getEncryptedPassword(pass);
            if (hash === resultHash){
                return true;
            } else {
                throw new Error(loginFailError);
            }

        } catch (e) {
            throw new Error(e.message);
        }
    }

    /**
     * generate and update user record
     * @param data
     * @returns {Promise.<T>}
     */
    async generateAuthToken (data) {
        try{
            let token = await jwt.sign({id: data.id, access: data.role}, hash.salt2);
            let update_resp = await this.update(data.id, {
                last_login: this.db.now().inTimezone('+05:00'),
                token: {
                    auth_token: token
                }
            });

            if(!update_resp.errors && update_resp.replaced){
                let redirect = '/';
                switch (data.role){
                    case 'Assistant':
                        redirect = '/assistant';
                        break;
                    case 'Administrator':
                        redirect = '/admin';
                        break;
                    case 'Operator':
                        redirect = `/${data.role_type.toLowerCase()}`;
                        break;
                    case 'Doctor':
                        redirect = '/doctor';
                        break;
                    default:
                        redirect = '/';
                }
                return {
                    token: token,
                    redirect: redirect,
                    selected_doctor: data.selected_doctor,
                    user_id: data.id
                };
            } else {
                throw new Error('Internal Server Error');
            }

        } catch (e){
            throw new Error(e.message);
        }
    }

    /**
     * return users list
     * @returns {Promise.<T>}
     */
    getUsers(){

        return this.db.table(this.table).orderBy(this.db.asc('login_id')).run().then( (data) => {
            return Promise.resolve(data);
        } )
            .catch((err) => {
                return Promise.reject(err);
            })

    }

    /**
     * add new user
     * @param data
     * @returns {Promise.<T>}
     */
    addNew (data) {
       return this.getEncryptedPassword(data.password).then( (hash) => {
           let data_to_insert = {};
           data_to_insert.password = hash;
           data_to_insert.login_id = data.login_id;
           data_to_insert.role = data.role;
           data_to_insert.role_type = data.role_type;
           data_to_insert.selected_doctor = data.selected_doctor;
           return this.insert(data_to_insert);
        })
            .catch(err => {
                return Promise.reject(err);
            })
    }

    /**
     * update user data
     * @param data
     * @returns {*}
     */
    updateData (data) {
        if (!_.isNil(data.password) && !_.isEmpty(data.password)) {
            return this.getEncryptedPassword(data.password).then(hash => {
                data.password = hash;
                return this.makeUpdate(data);
            }).catch(err => {
                    return Promise.reject(err);
                })
        } else {
            delete data.password;
            return this.makeUpdate(data);
        }
    }

    /**
     *
     * @param data
     * @returns {*}
     */
    makeUpdate (data) {
        return this.update(data.id,data);
    }

    /**
     * encrypt password string
     * @param pass
     * @returns {*}
     */
    async getEncryptedPassword (pass) {
        let encryptHash = await CryptoJS.SHA256(pass+hash.salt1).toString();
        if (encryptHash){
            return encryptHash;
        } else {
            throw new Error('Internal Server Error');
        }
    }

    /**
     * authenticate a user against its token
     * @param token
     * @param access
     * @returns {*}
     */
    async authenticateUser(token, access) {
        if (_.isNil(token)) {
            throw new Error();
        }
        try {
            let decoded = await jwt.verify(token, hash.salt2);
            let dbUser = await this.filter({id: decoded.id, role: access, token: {auth_token: token}});
            if(!_.isEmpty(dbUser)) {
                let user = dbUser[0];
                if (user.role === decoded.access) {
                    return true;
                } else {
                    throw new Error('Unauthorized Access');
                }
            } else {
                throw new Error('Unauthorized Access');
            }
        } catch (e) {
            throw new Error(e.message);
        }
    }

    async signOutUser (token) {
        let decoded = await jwt.verify(token, hash.salt2);
        try {
            let result = await this.update(decoded.id, {
                token: null
            });
            return result;
        } catch (e) {
            throw new Error(e.message);
        }
    }
}

module.exports = User;