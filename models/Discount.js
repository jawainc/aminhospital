/*********************
 *
 * Discount Model
 *
 *********************/
let BaseModel = require('./BaseModel');
let _ = require('lodash');
// table name
const table = 'discounts';
const timestamps = true;

class Discount extends BaseModel {

    constructor() {
        super(table, timestamps);
    }

    getChanges (io, socketUsers) {
        this.db.table(table).changes().run()
            .then(function(feed){
                let users = socketUsers.getDiscountUserList();
                let action = "";
                feed.each(function(err, item){
                    if (err) {
                        return;
                    } else if (_.has(item, 'new_val')) {
                        let dt = item.new_val;
                        let action = "";
                        if (_.has(dt, 'status') && dt.status === 'Pending'){
                            action = 'add';
                        } else {
                            action = 'sub';
                        }
                        if (users) {
                            users.forEach((id) => {
                                io.to(id).emit('discountChannel', action);
                            })
                        }
                    }
                });


        }).catch((error) => {
            console.log(error);
        });
    }

    addDiscount(data){
        if (this.lodash.has(data, 'patient_entry_id')) {
            data.status = 'Pending';
            data.created_at = new Date();
            data.updated_at = new Date();

            return this.db.branch(
                this.db.table(table).filter({'patient_entry_id': data.patient_entry_id}).isEmpty(),
                this.db.table(table).insert(data),
                false
            );
        } else {
            return Promise.reject('Bad Request');
        }
    }

    async getDiscountsNumbers () {
        try {
            let num = await this.db.table(table).filter({status: 'Pending'}).count().run();
            return Promise.resolve(num);
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async getDiscountList () {
        try {
            let list = await this.db.table(table).filter({status: 'Pending'}).orderBy('created_at').merge((row) => {
                return {
                    'patient': this.db.table('patient_entries').get(row('patient_entry_id')).do((p) => {
                        return this.db.table('patients').get(p('patient_id')).pluck('name')
                    })
                }
            });
            return Promise.resolve(list);
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async getDiscount (id) {
        try {
            let result = await this.db.table(table).get(id).merge((row) => {
                return {
                    'patient': this.db.table('patient_entries').get(row('patient_entry_id')).do((p) => {
                        return this.db.table('patients').get(p('patient_id')).pluck('name')
                    }),
                    'doctor': this.db.table('patient_entries').get(row('patient_entry_id')).do((p) => {
                        return this.db.table('doctors').get(p('doctor_id')).pluck('name')
                    })
                }
            });
            return Promise.resolve(result);
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async updateDiscount (id) {
        try {
            let result = await this.update(id, {status: 'Complete', updated_at: new Date()}, {returnChanges: true});
            if (result.replaced === 1) {
                let update =  result.changes[0].new_val;
                let p_e_id = update.patient_entry_id;
                
                let discount = parseInt(update.discount);
                let income = await this.db.table('incomes').filter({'patient_entry_id': p_e_id}).nth(0).run();

                let total_amount = income.total_amount - discount;
                let remaining_balance = income.total_amount - discount;
                let assistants = income.assistants;
                let income_data = {
                    total_amount: total_amount,
                    discount: discount,
                    doctor_share: 0,
                    department_share: 0,
                    assistants: [],
                    updated_at: new Date()
                };

                //calculate doctor share
                income_data.doctor_share = this.calcIncome({
                    total_amount: total_amount,
                    share: income.doctor_percentage
                });

                // update department income
                remaining_balance = remaining_balance - income_data.doctor_share;

                // calculate assistant shares
                _.forEach(assistants, (employee) => {
                    let asst_p = this.calcIncome({total_amount: total_amount, share: employee.percentage});
                    // update department income
                    remaining_balance = remaining_balance - asst_p;
                    income_data.assistants.push({
                        id: employee.id,
                        percentage: employee.percentage,
                        share: asst_p
                    })
                });

                income_data.department_share = remaining_balance;
                                
                let done = await this.db.table('incomes').get(income.id).update(income_data).run();
                return Promise.resolve(done);

            } else {
                return Promise.reject('Unable to update');
            }
        } catch (e) {
            console.log(e);
            return Promise.reject(e);
        }
    }

    calcIncome(data){
        return parseInt(data.total_amount * (data.share/100))
    }
}
module.exports = Discount;