const express = require('express');
const router = express.Router();

var _ = require('lodash');

var User = require('../../models/User');
var PatientEntry = require('../../models/PatientEntry');
var Income = require('../../models/Income');
var Dictionary = require('../../models/Dictionary');
var helper = require('../../shared/helpers');
const {hash} = require('../../config/config');

router.use(function (req, res, next) {
    if (!req.session && !req.session.sessionNumber && req.session.sessionNumber !== hash.salt3) {
        res.status(401).send('Unauthorized Access');
        res.end();
    }
    let token = req.header('x-auth');
    let user = new User();
    user.authenticateUser(token, 'Doctor').then(() => {
        next();
    }).catch(error => {
        res.status(401).send(error.message);
    })
});

router.get('/isDoctor', (req, res) => {
    res.status(200).send('OK');
});

router.get('/patients', (req, res) => {

    let pe = new PatientEntry();

    // pe.getChanges();

    let doctor_id = req.session.selected_doctor.doctor_id;
    let department_id = req.session.selected_doctor.department_id;

    pe.getDoctorPatients(doctor_id, department_id).then( (data) => {
        res.send(helper.returnSuccess("",data));
    })
        .catch(err => res.send(helper.returnFail("Error in finding patients<br>"+err)));

});

router.get('/patient/detail/:id', (req, res) => {
    let pe_id = req.params.id;
    let pe = new PatientEntry();

    pe.getDoctorPatientDetail(pe_id).then( (response) => {
        res.send(helper.returnSuccess("",response));
    })
        .catch(err => res.send(helper.returnFail("Error in finding patient<br>"+err)));

});

router.get('/assistant/patient/detail/:id', (req, res) => {
    let pe_id = req.params.id;
    let pe = new PatientEntry();

    pe.getAssistantPatientDetail(pe_id).then( (data) => {
        res.send(helper.returnSuccess("",data));
    })
        .catch(err => res.send(helper.returnFail("Error in finding patient<br>"+err)));

});

router.post('/assistant/patient/add', (req, res) => {
    let data = req.body;
    let pe = new PatientEntry();

    if (data.dictionaryData.length > 0) {
        let dict = new Dictionary();
        _.forEach(data.dictionaryData, function (row) {
            dict.add(row).then();
        });
    }

    pe.update(data.id, data.update_data)
        .then( success => {
            res.send(helper.returnSuccess("Saved Successfully", {redirect: '/doctor/patients'}));
        })
        .catch(err => res.send(helper.returnFail(err)));
});

router.get('/assistant/history/:id', (req, res) => {
    let pe_id = req.params.id;
    let pe = new PatientEntry();

    pe.getPatientHistory(pe_id).then( (data) => {
        res.send(helper.returnSuccess("",data));
    })
        .catch(err => res.send(helper.returnFail("Error in finding patient<br>"+err)));

});

router.get('/dash', (req, res) => {
    let pe = new Income();
    let doctor_id = req.session.selected_doctor.doctor_id;
    pe.getDashData(doctor_id).then( (data) => {
        res.send(helper.returnSuccess("",data));
    })
        .catch(err => res.send(helper.returnFail(err)));

});

module.exports = router;