var express = require('express');
var router = express.Router();

var Model = require('../../models/User');
var helper = require('../../shared/helpers');

// Signout
router.get('/', function(req, res) {
    req.session = null
    let e = new Model();
    let token = req.header('x-auth');
    e.signOutUser(token).then(response => {
        res.header('x-auth', {}).send(helper.returnSuccess('', {redirect: '/'}));
    }).catch(error => {
        res.status(400).send(error.message);
    });
});
module.exports = router;
