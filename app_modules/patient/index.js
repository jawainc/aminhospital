const express = require('express');
const router = express.Router();

var _ = require('lodash');

var Discount = require('../../models/Discount');
var User = require('../../models/User');
var helper = require('../../shared/helpers');
const {hash} = require('../../config/config');

router.use(function (req, res, next) {
    if (!req.session && !req.session.sessionNumber && req.session.sessionNumber !== hash.salt3) {
        res.status(401).send('Unauthorized Access');
        res.end();
    }
    let token = req.header('x-auth');
    let user = new User();
    user.authenticateUser(token, 'Doctor').then(() => {
        next();
    }).catch(error => {
        res.status(401).send(error.message);
    })
});

router.post('/add/discount', (req, res) => {
    let data = req.body;
    let di = new Discount();


    di.addDiscount(data).then((result) => {
        if (result !== false)
            res.send(helper.returnSuccess('Added Successfully'));
        else
            res.send(helper.returnFail('Already discounted'));


    }).catch(error => {
        res.send(helper.returnFail(error));
    });

});


module.exports = router;