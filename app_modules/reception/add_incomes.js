var Income = require('../../models/Income');
var DoctorIncome = require('../../models/DoctorIncome');
var EmployeeIncome = require('../../models/EmployeeIncome');
var DepartmentIncome = require('../../models/DepartmentIncome');
var Department = require('../../models/Department');
var _ = require('lodash');
let inc = new Income();
let doc_inc = new DoctorIncome();
let emp_inc = new EmployeeIncome();
let dep_inc = new DepartmentIncome();
let dept = new Department();
let BaseModel = require('../../models/BaseModel');

class AddIncomes extends BaseModel{



    async addIncome(data){

        try {

            let departmentIncome = data.total_amount;

            let percentages = await this.db.table('departments').get(data.department_id)('doctors')
                .filter((doc) => {
                    return doc('id').eq(data.doctor_data.id)
                }).nth(0).pluck('id', 'percentage', 'assistants').run();

            let income_data = {
                patient_entry_id: data.patient_entry_id,
                department_id: data.department_id,
                total_amount: data.total_amount,
                fee: data.fee,
                discount: data.discount,
                doctor_id: percentages.id,
                doctor_share: 0,
                doctor_percentage: percentages.percentage,
                department_share: 0,
                assistants: [],
                created_at: new Date(),
                updated_at: new Date()
            };
            //calculate doctor share
            income_data.doctor_share = this.calcIncome({
                total_amount: data.total_amount,
                share: percentages.percentage
            });
            // update department income
            departmentIncome = departmentIncome - income_data.doctor_share;

            // calculate assistant shares
            _.forEach(percentages.assistants, (employee) => {
                let asst_p = this.calcIncome({total_amount: data.total_amount, share: employee.percentage});
                // update department income
                departmentIncome = departmentIncome - asst_p;
                income_data.assistants.push({
                    id: employee.id,
                    percentage: employee.percentage,
                    share: asst_p
                })
            });

            income_data.department_share = departmentIncome;

            let income = await this.db.table('incomes').insert(income_data).run();

            return Promise.resolve(income);
        } catch (e) {
            return Promise.reject(e);
        }

    }

    calcIncome(data){
        return parseInt(data.total_amount * (data.share/100))
    }
}
module.exports = AddIncomes;