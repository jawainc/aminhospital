const express = require('express');
const router = express.Router();

let _ = require('lodash');

let Patient = require('../../models/Patient');
let Departments = require('../../models/Department');
let PatientEntry = require('../../models/PatientEntry');
let Discount = require('../../models/Discount');
let Expanse = require('../../models/Expanse');
let AddIncome = require('./add_incomes');
let helper = require('../../shared/helpers');
let User = require('../../models/User');
const {hash} = require('../../config/config');

router.use(function (req, res, next) {
    if (!req.session && !req.session.sessionNumber && req.session.sessionNumber !== hash.salt3) {
        res.status(401).send('Unauthorized Access');
        res.end();
    }
    let token = req.header('x-auth');
    let user = new User();
    user.authenticateUser(token, 'Operator').then(() => {
        next();
    }).catch(error => {
        res.status(401).send(error.message);
    })
});

router.get('/isReception', (req, res) => {
    res.status(200).send('OK');
});

router.get('/patients', (req, res, next) => {
});

router.get('/outdoor/doctors', (req, res) => {
    let model = new Departments();
    model.getDepartmentsForReception()
        .then(data => {
            res.send(helper.returnSuccess("",data));
        })
        .catch((err) => res.status(500).send(err));
});

router.get('/outdoor/phones/:phone', (req, res) => {
    let p = new Patient();
    let data = {phone: req.params.phone};
    p.getPhoneData(data).then( (data) => res.send(helper.returnSuccess("",data)) )
        .catch( (err) => res.status(500).send(err) );

});

router.get('/last-visit/:id', (req, res) => {
    let p = new PatientEntry();
    p.getLastVisit(req.params.id).then( (data) => res.send(helper.returnSuccess("",data)) )
        .catch( (err) => res.status(500).send(err) );

});

router.post('/outdoor/patient/add',(req, res) => {
    let data = req.body;
    let department = new Departments();
    let p = new Patient();
    let pe = new PatientEntry();
    let add_inc = new AddIncome();
    let status = '';
    department.hasAssistantForm(data.doctor.department_id, data.doctor.id).then(success => {
       if (success) {
           status = 'Doctor Pending';
       } else {
           status = 'Pending';
       }
    },
    error => {
        status = 'Doctor Pending';
    });

    p.addPatient(data.patient)
        .then( p_id => {
            return {
                patient_id: p_id,
                department_id: data.doctor.department_id,
                type: "Out Door",
                doctor_id: data.doctor.id,
                status: status,

                prescription: {}
            }
        })
        .then(pe_data => {
            return pe.insert(pe_data);
        })
        .then(result => {
            if (result.inserted) {
                return {
                    patient_entry_id: result.generated_keys[0],
                    department_id: data.doctor.department_id,
                    total_amount: data.total.total,
                    fee: data.doctor.fee,
                    discount: data.total.discount,
                    doctor_data: {
                        id: data.doctor.id,
                        name: data.doctor.name
                    }
                };
            } else {
                return Promise.reject("Patient Entry Failed")
            }
        })
        .then(data => {
            add_inc.addIncome(data)
                .then(success => {
                return Promise.resolve();
            })
                .catch(error => {
                    return pe.delete(data.patient_entry_id)
                        .then(deleteSuccess => {
                            return Promise.reject(error);
                        })
                        .catch(error=> {
                            return Promise.reject(error);
                        });
                });
        })
        .then(success => {
            res.send(helper.returnSuccess("Patient Saved"));
            res.end();
        })
        .catch(error => {
            res.send(helper.returnFail(error));
            res.end();
        })
});

router.get('/discount/num', (req, res) => {
    let model = new Discount();
    model.getDiscountsNumbers()
        .then(data => {
            res.send(helper.returnSuccess("",data));
        })
        .catch((err) => res.status(500).send(err));
});

router.get('/discount/list', (req, res) => {
    let model = new Discount();
    model.getDiscountList()
        .then(data => {
            res.send(helper.returnSuccess("",data));
        })
        .catch((err) => res.status(500).send(err));
});

router.get('/discount/detail/:id', (req, res) => {
    let model = new Discount();
    let id = req.params.id;
    model.getDiscount(id)
        .then(data => {
            res.send(helper.returnSuccess("",data));
        })
        .catch((err) => res.status(500).send(err));
});

router.put('/discount/update', (req, res) => {
    let data = req.body;
    let model = new Discount();
    model.updateDiscount(data.id)
        .then(data => {
            res.send(helper.returnSuccess("",data));
        })
        .catch((err) => res.status(500).send(err));
});

router.post('/expanse/save', (req, res) => {
    let data = req.body;
    let model = new Expanse();
    model.insert(data).then(data => {
        res.send(helper.returnSuccess("Saved Successfully",data));
    })
        .catch((err) => res.status(500).send(err));
});

module.exports = router;