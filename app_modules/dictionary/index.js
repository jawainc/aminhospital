const express = require('express');
const router = express.Router();

var _ = require('lodash');
var Dictionary = require('../../models/Dictionary');
var helper = require('../../shared/helpers');

/*router.use(function (req, res, next) {
    console.log("Assistant Middleware");
    let token = req.header('x-auth');
    let user = new User();
    user.authenticateUser(token, 'Assistant').then(() => {
        console.log("Next");
        next();
    }).catch(error => {
        console.log("Rejecting");
        res.status(401).send('Unauthorized Access');
    })
});*/

router.get('/all/:name/:query', (req, res) => {

    let data_search_dict = {
        [req.params.name]: req.params.query
    };
    let dic = new Dictionary();

    dic.search(data_search_dict).then(data => {
        console.log(data);
        res.send(helper.returnSuccess("",data));
    }, error => {
        console.log(error);
        res.send(helper.returnFail("finding dictionary"));
    });


});

router.post('/add', (req, res) => {
    let data = req.body;
    let pe = new PatientEntry();

    pe.update(data.id, data.update_data)
        .then( success => {
            res.send(helper.returnSuccess("Saved Successfully", {redirect: '/assistant/patients'}));
        })
        .catch(err => res.send(helper.returnFail(err)));
});

module.exports = router;