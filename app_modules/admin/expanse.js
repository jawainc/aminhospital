const express = require('express');
const router = express.Router();

var Model = require('../../models/Expanse');
var helper = require('../../shared/helpers');

router.post('/details', (req, res) => {
    let expanse = new Model();
    let from = new Date(req.body.from);
    let to = new Date(req.body.to);
    expanse.getExpanseDetails(from, to).then((data) => {
        res.send(helper.returnSuccess("", data));
    }).catch( (err) => res.status(500).send(err) );
});

module.exports = router;