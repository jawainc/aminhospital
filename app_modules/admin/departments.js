const express = require('express');
const router = express.Router();

var Model = require('../../models/Department');
var helper = require('../../shared/helpers');



/**
 * Get
 */
router.get('/get/:id', (req, res) => {
    let e = new Model();

    let id = req.params.id;

    e.find(id)
        .then( (data) => res.send(helper.returnSuccess("",data)) )
        .catch( (err) => res.status(500).send(err) );
});



/**
 * Get has many with embeded relation
 */
router.get('/get/embeded/:id', (req, res) => {
    let e = new Model();

    let id = req.params.id;

    e.find_with_relation_embeded(id)
        .then( (data) => res.send(helper.returnSuccess("",data)) )
        .catch( (err) => res.status(500).send(err) );
});





/**
 * Get all
 */
router.get('/all', (req, res) => {
    let e = new Model();
    e.get({order: "asc", col: "name"})
        .then( (data) => res.send(helper.returnSuccess("",data)) )
        .catch( (err) => res.status(500).send(err) );
});

/**
 * Add new
 */
router.post('/add',(req, res) => {
    let data = req.body;
    var e = new Model();


    e.insert(data)
        .then((data) => {
            res.send(helper.returnSuccess("Created Successfully"));
        })
        .catch((err) => res.send(helper.returnFail(err)));
});


/**
 * Update
 */
router.put('/update',(req, res) => {
    let data = req.body;
    var e = new Model();

    e.update(data.id,data)
        .then((data) => {
            res.send(helper.returnSuccess("Updated Successfully"));
        })
        .catch((err) => res.send(helper.returnFail(err)));
});


/**
 * Delete record
 */
router.delete('/delete/:id',(req, res) => {
    let id = req.params.id;
    var e = new Model();

    e.delete(id)
        .then( (data) => {


            if(data.deleted)
                res.send(helper.returnSuccess("Deleted Successfully"))
            else
                res.send(helper.returnFail("Unable to delete"))
        })
        .catch( (err) => res.status(500).send(err) );

});

router.post('/check/unique/insert', (req,res) => {
    let data = req.body;
    var e = new Model();

    console.log(data);

    e.checkUniqueInsert(data)
        .then((unique) => {

            if(!unique)
                res.send(helper.returnSuccess("Not Exists"));
            else
                res.send(helper.returnFail("Already Exists"));
        })
        .catch((err) => res.status(500).send(err))

});
router.post('/check/unique/update', (req,res) => {
    let data = req.body;
    var e = new Model();

    e.checkUniqueUpdate(data.data, data.id)
        .then((unique) => {

            if(!unique)
                res.send(helper.returnSuccess("Not Exists"));
            else
                res.send(helper.returnFail("Already Exists"));
        })
        .catch((err) => res.status(500).send(err))

});

module.exports = router;