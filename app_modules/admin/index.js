const express = require('express');
const admin_app = express();
const User = require('../../models/User');

admin_app.use(function (req, res, next) {
    if (!req.session && !req.session.sessionNumber && req.session.sessionNumber !== hash.salt3) {
        res.status(401).send('Unauthorized Access');
        res.end();
    }
    let token = req.header('x-auth');
    let user = new User();
    user.authenticateUser(token, 'Administrator').then(() => {
        next();
    }).catch(error => {
        res.status(401).send(error.message);
    })
});

admin_app.get('/isAdmin', function(req, res, next) {
    res.status(200).send('OK');
});

let doctors = require('./doctors');
let departments = require('./departments');
let employees = require('./employees');
let users = require('./user');
let income = require('./income');
let expanse = require('./expanse');

admin_app.use('/departments', departments);
admin_app.use('/doctors', doctors);
admin_app.use('/employees', employees);
admin_app.use('/users', users);
admin_app.use('/incomes', income);
admin_app.use('/expanse', expanse);

// catch 404 and forward to error handler
admin_app.use(function(req, res, next) {
    res.status(404).send('Not Found');
});

module.exports = admin_app;