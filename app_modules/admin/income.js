const express = require('express');
const router = express.Router();

var Model = require('../../models/Income');
var helper = require('../../shared/helpers');

router.post('/balance', (req, res) => {
    let income = new Model();
    let from = new Date(req.body.from);
    let to = new Date(req.body.to);
    income.getBalance(from, to).then((data) => {
        res.send(helper.returnSuccess("", data));
    }).catch( (err) => res.status(500).send(err) );
});

router.post('/deposits', (req, res) => {
    let income = new Model();
    let from = new Date(req.body.from);
    let to = new Date(req.body.to);
    income.getDeposit(from, to).then((data) => {
        res.send(helper.returnSuccess("", data));
    }).catch( (err) => res.status(500).send(err) );
});


module.exports = router;