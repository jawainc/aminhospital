var express = require('express');
var router = express.Router();

var Model = require('../../models/User');
var helper = require('../../shared/helpers');
let {hash} = require('../../config/config');

// Auth login
router.post('/auth', function(req, res) {
    let e = new Model();
    e.login(req.body).then(response => {
        req.session.sessionNumber = hash.salt3;
        req.session.selected_doctor = response.selected_doctor;
        res.header('x-auth', response.token).send(helper.returnSuccess('', {redirect: response.redirect}));
    }).catch(error => {
        res.status(400).send(error.message);
        });
});
module.exports = router;
