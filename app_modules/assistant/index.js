const express = require('express');
const router = express.Router();

var _ = require('lodash');
var PatientEntry = require('../../models/PatientEntry');
var Dictionary = require('../../models/Dictionary');
var User = require('../../models/User');
var helper = require('../../shared/helpers');
const {hash} = require('../../config/config');


router.use(function (req, res, next) {
    if (!req.session && !req.session.sessionNumber && req.session.sessionNumber !== hash.salt3) {
        res.status(401).send('Unauthorized Access');
        res.end();
    }
    let token = req.header('x-auth');
    let user = new User();
    user.authenticateUser(token, 'Assistant').then(() => {
        next();
    }).catch(error => {
        res.status(401).send(error.message);
    })
});

router.get('/isAssistant', (req, res) => {
    res.status(200).send('OK');
});

router.get('/patients', (req, res) => {

    let pe = new PatientEntry();

    // pe.getChanges();

    let doctor_id = req.session.selected_doctor.doctor_id;
    let department_id = req.session.selected_doctor.department_id;

    pe.getDoctorPatients(doctor_id, department_id).then( (data) => {
        res.send(helper.returnSuccess("",data));
    })
        .catch(err => res.send(helper.returnFail("Error in finding patients<br>"+err)));

});

router.get('/patient/detail/:id', (req, res) => {
    let pe_id = req.params.id;
    let pe = new PatientEntry();

    pe.getAssistantPatientDetail(pe_id).then( (data) => {
        res.send(helper.returnSuccess("",data));
    })
        .catch(err => res.send(helper.returnFail("Error in finding patient<br>"+err)));
});

router.post('/patient/add', (req, res) => {
    let data = req.body;
    let pe = new PatientEntry();

    if (data.dictionaryData.length > 0) {
        let dict = new Dictionary();
        _.forEach(data.dictionaryData, function (row) {
           dict.add(row).then();
        });
    }

    pe.update(data.id, data.update_data)
        .then( success => {
            res.send(helper.returnSuccess("Saved Successfully", {redirect: '/assistant/patients'}));
        })
        .catch(err => res.send(helper.returnFail(err)));
});

module.exports = router;