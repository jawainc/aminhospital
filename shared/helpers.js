class Helpers{

    /**
     *
     * @param msg
     * @param dt
     * @returns {{data: {status: string, msg: string, data: *}}}
     */
    static returnSuccess(msg = "", dt = {}){
        return {

                status: true,
                msg: msg,
                data: dt

        }
    }

    /**
     *
     * @param msg
     * @returns {{data: {status: string, msg: string}}}
     */
    static returnFail(msg = ""){
        return {

                status: false,
                msg: msg,

        }
    }

}

module.exports = Helpers;